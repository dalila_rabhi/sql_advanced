

1 --------------------------------

select p2.person_id, p2.person_name, p2.health_status
from  person as p1,  person as p2, visit as v1, visit as v2 
where p1.person_name = 'Landyn Greer' 
and p1.person_id <> p2.person_id
and v1.person_id = p1.person_id
and v2.person_id = p2.person_id
and v1.place_id = v2.place_id
and ((v1.start_datetime >= v2.start_datetime and v1.start_datetime <= v2.end_datetime) or (v1.end_datetime >= v2.start_datetime and v1.end_datetime <= v2.end_datetime))

2 --------------------------------
select count(distinct p1.person_id)
from  person as p1,  person as p2, visit as v1, visit as v2, place as pl1,  place as pl2   
where p1.health_status = 'Sick' 
And pl1.place_type = 'Bar'
and p1.person_id <> p2.person_id
and v1.person_id = p1.person_id
and v2.person_id = p2.person_id
and v1.place_id = v2.place_id
and pl2.place_id = v2.place_id
and pl1.place_id = v1.place_id
and ((v1.start_datetime >= v2.start_datetime and v1.start_datetime <= v2.end_datetime) or (v1.end_datetime >= v2.start_datetime and v1.end_datetime <= v2.end_datetime))


3-------------------------------

select p2.person_id, p2.person_name
from  person as p1,  person as p2, visit as v1, visit as v2 
where p1.person_name = 'Taylor Luna' 
and p1.person_id <> p2.person_id
and v1.person_id = p1.person_id
and v2.person_id = p2.person_id
and v1.place_id = v2.place_id
and ((v1.start_datetime >= v2.start_datetime and v1.start_datetime <= v2.end_datetime) or (v1.end_datetime >= v2.start_datetime and v1.end_datetime <= v2.end_datetime))


4 --------------------------------
select count(p.person_id) as nbr_malade, pl.place_name, avg(v.end_datetime-v.start_datetime) as duree_moyenne  
from person as p, visit as v,  place as pl 
where p.health_status = 'Sick'
and v.person_id = p.person_id 
and pl.place_id = v.place_id 
group by pl.place_name 

 --------------------------------
5 select count(p.person_id) as nbr_sains, pl.place_name, avg(v.end_datetime-v.start_datetime) as duree_moyenne  
from person as p, visit as v,  place as pl 
where p.health_status = 'Healthy'
and v.person_id = p.person_id 
and pl.place_id = v.place_id 
group by pl.place_name 
