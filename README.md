

# TD sql advanced

A rendre pour le 30/10/2022

## Consignes générales

-   CHAQUE RESULTAT DOIT ETRE EXPORTE EN .csv, RENOMME, ENREGISTRE DANS
    UN DOSSIER 'RESULTAT'
-   Les deux scripts sont aussi enregistrés
-   Ajout sur git : Frok, git clone, git push (une fois terminé)

### Données

-   Contact tracing : données suivies des personnes
    -   Person.csv : 1 ligne par patient (id, nom, status de santé)

    -   Visit.csv : 1 ligne par visite d'un lieu (id de la visite, id du
        patient visiteur, id de l'endroit de la visite, date de début de
        la visite, date de fin de la visite)

    -   Place.csv : 1 ligne par lieu (id, nom du lieu, type du lieu)

### Script DDL

-   Creation des tables
-   Insertion des données
-   Clés primaires, clés étrangères

### Requetes

-   Noms + statuts santé des personnes que Landyn Greer a croisé (même
    lieu, même moment)
-   Nombre de malades croisés dans un bar (même moment)
-   Noms des personnes que Taylor Luna a croisé


-   Nombre de malades par endroit (place_name) + durée moyenne de leurs visites dans cet endroit
-   Nombre de sains (non malades) par endroit (place_name) + durée moyenne de leurs visites dans cet endroit
