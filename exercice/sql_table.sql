---Créer les tables : 

create TABLE person(
     person_id    INT PRIMARY KEY  NOT NULL,
     person_name   VARCHAR(20) NOT NULL,
     health_status VARCHAR(20) NOT null,
     confirmed_status_date TIMESTAMP NOT NULL  ); 
  
  ------------------------------------------------------------

CREATE table place (
     place_id      INT  NOT NULL,
     place_name   VARCHAR(20)  NOT NULL,
     place_type  VARCHAR(20) NOT null
   );
  alter table place 
add CONSTRAINT pk_place PRIMARY KEY  (place_id)

-------------------------------------------------------------
CREATE TABLE visit (
     visit_id  INT PRIMARY KEY  NOT NULL,
     person_id     INT 	NOT NULL,
     place_id     INT NOT null, 
     start_datetime    timestamp		NOT NULL,
     end_datetime     		timestamp	NOT NULL 
     );
alter table visit
add CONSTRAINT fk_visit_person foreign key (person_id) references person (person_id)
 alter table visit 
add CONSTRAINT fk_visit_place foreign key (place_id) references place (place_id)
  
 